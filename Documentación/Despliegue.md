# Despliegue
El objetivo de este documento es realizar un an�lisis de los diferentes requisitos y decisiones
t�cnicas necesarias para desplegar nuestra aplicaci�n, Filmedom.

Con el despliegue de la app se tiene que conseguir que los componentes de esta sean escalables
en un futuro y, adem�s, que se encuentre con una alta disponibilidad. En base a estas dos
necesidades, se va a desarrollar el documento del despliegue propuesto.

## Consideraciones previas
---------------------
Antes de pensar como se va a realizar el despliegue hay que definir varios conceptos que van a
marcar el camino. Dentro de este punto entran los dos requerimientos no funcionales ya comentados,
escalabilidad y alta disponibilidad.

Relacionado con el tema de la alta disponibilidad, se va a suponer que la aplicaci�n solo va a
utilizarse en Espa�a y como mucho, dentro de Europa, lo que reduce las zonas a las que se debe
llegar con la aplicaci�n.

Por otra parte, el dise�o de la arquitectura de la aplicaci�n, esta dividido en microservicios, lo
que hace que su escalado sea bastante sencillo, ya que al estar tan modularizada, cada uno de los
elementos de la aplicaci�n puede crecer y reducirse seg�n se necesite.

Por �ltimo, las consideraciones sobre el presupuesto, ya que al desplegar la aplicaci�n, ya sea en
la nube o en un data center propio, es necesaria una inversi�n, que, en nuestro caso, para el
despliegue de la app al mercado, el presupuesto inicial es relativamente alto, lo que quiere decir
que no tenemos que preocuparnos demasiado por este.

## Entornos donde se va a desplegar
---------------------
Ser� necesario contar con diferentes entornos para poder desarrollar nuestra aplicaci�n y realizar
pruebas antes de publicar cada peque�o cambio. Estos entornos ser� los siguientes:

### Pre-Producci�n
Este entorno es el utilizado para las pruebas unitarias de la aplicaci�n. Este entorno ser� utilizado
por la pipeline de Jenkins para ejecutar los tests necesarios antes de desplegarlo en el entorno
de producci�n (Siempre que la salida de estos sea positiva).

En esta rama no sera necesario que cumpla los requerimientos de la aplicaci�n final, as� que puede
ejecutarse directamente en el servidor que est� alojado Jenkins mediante im�genes de Docker.

Los datos que utiliza pueden ser una copia de los de la base de datos de producci�n o una serie de datos
de prueba. Lo importante es que las pruebas no alteren los datos que se encuentran en las Bases de
Datos del entorno de producci�n.

### Producci�n
Este sera el entorno de la aplicaci�n final y que se pretende que siga un proceso de integraci�n y
mejora continua. Cuando un desarrollador realiza un commit en la rama master de un servicio y 
este pasa todas las pruebas unitarias, Jenkins realizara el despliegue de la aplicaci�n en este
entorno.

Los requerimientos de este entorno son los definidos al inicio del documento, y la forma que se va
a construir y desplegar est� explicado en los siguientes puntos de este. Resumiendo, este entorno
debe cumplir que est� desplegado con una alta disponiblidad y ademas es escalable con respecto
a la carga que recibe.

Con el fin de cumplir estos requerimientos, estar� alojado en AWS.

### AWS
AWS es una infraestructura de la nube que ofrece Amazon que ofrece distintos servicios de computaci�n
en la nube. Se ha decidido utilizarlo debido a su flexibilidad y facilidad para desplegar aplicaciones
que cumplen los requisitos que tiene nuestra Web-App.

## Componentes y arquitectura de la aplicaci�n para el despliegue
---------------------
### Arquitectura de despliegue
![Despliegue](Assets/Despliegue.png)

En la imagen superior se observa la arquitectura de despliegue en AWS de nuestra aplicaci�n. Con el fin de
cumplir todos los requisitos de la aplicaci�n se han utilizado diferentes componentes que permiten
que la aplicaci�n sea auto escalable y con alta disponibilidad.

B�sicamente, se utilizan tres VPC diferentes, una para cada uno de los microservicios en donde:

- La VPC de la aplicaci�n o frontend estar� expuesta a internet y utilizara un internet gateway para
que los usuarios puedan acceder.
- Las otras dos VPC son locales y contienen los servicios de pel�culas y series y se comunican con la primera
VPC y con las bases de datos.
- Cada una de las VPC tienen un balanceador de carga que a cada una de las peticiones que llegan, las
distribuyen entre las diferentes instancias creadas para que la aplicaci�n sea escalable.
- Todas las conexiones entre las VPC y dentro de cada una de ellas son seguras.

Las diferentes instancias que se crean para cada microservicio est�n dentro de un grupo de autoescalado,
lo que hace que adem�s, la aplicaci�n sea tolerante a fallos de instancia (Replicado de estas). Por otra parte,
el grupo de auto escalado permite que, cuando la app lo necesite y seg�n una serie de reglas establecidas,
se creen nuevas instancias o se vuelvan a levantar las que se han ca�do.

>Por �ltimo, se han establecido dos zonas de disponibilidad de AWS diferentes:
>
>- eu-west-1: Irlanda
>- eu-west-3: Paris

Los grupos de auto escalado est�n distribuidos dentro de las tres zonas de disponibilidad que existen,
en nuestro caso,dentro de eu-west-1, en principio se utilizar�n

- eu-west-1a
- eu-west-1b

La forma que se ha establecido que se va a trabajar con estas zonas de disponibilidad es:

- El n�mero de **instancias preferidas es 2**, pudiendo llegar a ser mas de 2 (Regla del grupo de auto escalado).
- El grupo de auto escalado de AWS debe distribuir las instancias que levanta entre las dos zonas
establecidas, de forma que tiene que haber como **m�nimo una en cada zona**.
- En cada una de las zonas de disponibilidad, la base de datos estar� replicada, por lo que deber� existir
una forma de **consistencia eventual** entre las dos bases de datos (Si no hay actualizaciones, todas las 
peticiones terminar�n devolviendo el �ltimo valor actualizado).

Por �ltimo, la aplicaci�n utiliza im�genes docker para las instancias para facilitar su
despliegue y escalado.

Con todos estos requerimientos se consiguen los requerimientos propuestos:

- **Alta disponibilidad**. Al utilizar diferentes zonas de disponibilidad de AWS, se conseguimos aprovecharnos
de la redundancia geogr�fica de los servidores de Amazon de forma que si una instalaci�n falla, la aplicaci�n
seguir� disponible en la segunda zona de disponibilidad. En nuestro caso, se utilizan las instalaciones de
Irlanda y de Par�s, que est�n bastante lejos entre si, por lo que si, por ejemplo, en Irlanda hay un terremoto 
que inhabilita las instalaciones, es muy dif�cil que la de Par�s tambi�n haya ca�do.
- **Escalado**. La aplicaci�n se va a desplegar mediante imagenes docker y se va a utilizar el grupo de
auto escalado de AWS. De esta forma, se garantiza un replicado de las im�genes docker en diferentes instancias
en AWS, que, junto a los balanceadores de carga (que distribuyen las peticiones a las diferentes instancias), 
consiguen el escalado de la aplicaci�n que se quer�a.

### Componentes AWS a utilizar
- ** Internet Gateway**. Es el componente que permite la comunicaci�n entre nuestra VPC e internet. Cuenta con
una tabla de enrutamiento, con lo que hace la funci�n de un router. Adem�s realizar traducciones NAT. En nuestro
caso solo distribuir� mensajes entre el primer balanceador de carga y el exterior.
- **Instancias EC2**. Es una m�quina virtual en la que se ejecuta el c�digo de nuestra aplicaci�n. Cada micro-servicio
se ejecutar� en una instancia EC2 diferente (dos para alta disponibilidad). Hay que tener en cuenta que cada
instancia de EC2 puede estar ejecut�ndose en una misma m�quina f�sica con instancias de otros usuarios, lo que
puede provocar que el rendimiento de nuestra aplicaci�n fluct�e. Existe la posibilidad de alojarlas en lo que
AWS llama "Dedicated instances", que son m�quinas f�sicas dedicadas exclusivamente a un cliente, sin embargo, su
precio es bastante superior a la opci�n por defecto y hemos decidido no usarlas.
- **Load balancer** de aplicaci�n y de network. El load balancer como aplicaci�n se utilizara una vez llega una
petici�n a la aplicaci�n y se comunica con lo que es el frontend de esta. Los load balancers de network de la
arquitectura se utilizan para distribuir la carga cuando llega a las instancias EC2 de los microservicios de
pel�culas y series.
- **Grupos de auto escalado**. Cuando una instancia EC2 no es capaz de soportar la carga, de forma autom�tica,
AWS crea una nueva instancia para distribuir la carga entre las dos.
- **VPC**. Grupo de subredes donde se encuentra cada una de las instancias de la aplicaci�n.
- **RDS**. Relational Database Service permite utilizar y escalar bases de datos relacionales. Nos proporciona
varios motores de bases de datos conocidos entre los que se encuentra MySQL, que es f�cilmente integrable con 
NodeJS. Amazon RDS usa la funcionalidad de replicaci�n integrada para crear un tipo especial de instancia de base
de datos, llamada r�plica de lectura, a partir de una instancia de base de datos de origen. Las actualizaciones
realizadas en la instancia de base de datos de origen se copian de forma as�ncrona en la r�plica de lectura. Puede
reducir la carga de la instancia de base de datos de origen dirigiendo las consultas de lectura de sus aplicaciones
a la r�plica de lectura. Esta funcionalidad resultar� muy �til en nuestra aplicaci�n porque las BBDDs de pel�culas y
series ser�n mayormente de lectura.