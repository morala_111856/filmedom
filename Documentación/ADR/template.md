# [T�tulo problema]
- Estado: [Propuesto | Rechazado | Aceptado]
- Participantes de la decisi�n: Javier Diez, Javier Morala
- Fecha: 2020-03-26

## Contexto y Problema
----------------------
[Describir el problema y su contxto, e.g., in free form using two to three sentences. You may want to articulate the problem in form of a question.]

## Controladores de la Decisi�n
----------------------
- [driver 1, e.g., a force, facing concern, �]
- [driver 2, e.g., a force, facing concern, �]
- �

## Opciones Consideradas
----------------------
- [option 1]
- [option 2]
- [option 3]
- �

## Decisi�n
----------------------
Opci�n elegida: "[option 1]", porque [justification. e.g., only option, which meets k.o. criterion decision driver | which resolves force force | � | comes out best (see below)].

### Consecuencias Positivas
- [e.g., improvement of quality attribute satisfaction, follow-up decisions required, �]
- �

### Consecuencias Negativas
- [e.g., compromising quality attribute, follow-up decisions required, �]
- �

## Pros y Contras de las Opciones
----------------------
### [option 1]
[example | description | pointer to more information | �]
- Good, because [argument a]
- Good, because [argument b]
- Bad, because [argument c]
- �

### [option 2]
[example | description | pointer to more information | �]
- Good, because [argument a]
- Good, because [argument b]
- Bad, because [argument c]
- �

### [option 3]
[example | description | pointer to more information | �]
- Good, because [argument a]
- Good, because [argument b]
- Bad, because [argument c]
- �

## Links
----------------------
- [Titulo del link]()
- �
