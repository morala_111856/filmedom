# Gesti�n de la Configuraci�n en el Proyecto
- Estado: Aceptado
- Participantes de la decisi�n: Javier Diez, Javier Morala
- Fecha: 2020-05-09

## Contexto y Problema
----------------------
Se necesita llevar una gesti�n de los diferentes procesos que se encargan de asegurar la calidad del
proyecto.

Esta gesti�n debe de abarcar tanto el c�digo del proyecto, como los diferentes documentos que se
generen para la consecuci�n de este.

En el caso del servicio que se quiere desarrollar, es necesario que nuestro gestor de configuraciones
sea capaz de incluir una serie de herramientas que sirvan para que todos los miembros del equipo
sean capaces de saber en todo momento las tareas que est�n realizando el resto de los compa�eros.

Tambi�n es necesario que posibilite una gesti�n de las diferentes fases de un documento antes de
ser terminado.

## Controladores de la Decisi�n
----------------------
- Necesidad 1, Control de Versiones
- Necesidad 2, Control de Cambios
- Necesidad 3, Gesti�n de Documentaci�n
- Necesidad 4, Gesti�n del equipo del del proyecto

## Opciones Consideradas
----------------------
### Gesti�n del proyecto
- Azure DevOps
- Jira
- Trello
- Bugzilla
- Asana
### Control de cambios y versiones
- BitBucket
- GitHub

## Decisi�n
----------------------
Opci�n elegida: **Jira + BitBucket**, porque gracias a sus diferentes herramientas, vamos a ser capaces
de cumplir con todas las necesidades propuestas. **BitBucket** se encarga del control de versiones,
mientras que con **Jira** se puede llevar una gesti�n efectiva de todo el proyecto por parte de todo el
equipo.

### Consecuencias Positivas
- Todo el equipo del proyecto integrado
- Separado por necesidades/tareas de gesti�n
- Interfaz web, por lo tanto, f�cilmente accesible.
- Integraci�n gesti�n de versiones y cambios con otras herramientas
- Misma empresa que desarrolla los dos softwares.

### Consecuencias Negativas
- Interfaz algo complicada la primera vez que se accede, entonces el equipo se tendr� que aprender a usarlo
- Necesidad de usar varias herramientas

## Pros y Contras de las Opciones
----------------------
### Azure DevOps
Herramienta para una gesti�n integral del proyecto software.

- OK, porque es f�cilmente integrable con repositorios.
- OK, porque permite una gesti�n �gil del proyecto con informes y gr�ficas.
- OK, porque permite una inclusi�n sencilla para la mejora continua.
- No OK, porque la mayor�a de sus ventajas no son gratis.

### Jira
Herramienta colaborativa para una gesti�n �gil de todo el proyecto a lo largo de su ciclo de vida.

- OK, porque permite planificar y supervisar el trabajo de los equipos
- OK, porque permite integrarse con Bitbucket o GitHub
- OK, porque permite una gesti�n de incidencias
- OK, porque tiene una funci�n para generar gr�ficos e informes del proceso.

### Trello
Herramienta colaborativa para una gesti�n �gil de los proyectos.

- OK, porque es f�cil de gestionar y configurar.
- OK, porque puede integrarse con BitBucket o GitHub
- OK, porque permite una gesti�n �gil del proyecto
- No OK, porque no permite una gesti�n de incidencias
- No OK, porque no tiene charts de trabajo

### Bugzilla
Herramienta para ayudar en la gesti�n del desarrollo del software.

- OK, porque es gratis.
- OK, porque permite llevar una gesti�n de errores e incidencias de una manera sencilla y eficiente.
- No OK, porque es necesario su instalaci�n en un servidor.
- No OK, porque no permite una planificaci�n y creaci�n de informes agiles.
- No OK, porque no tiene tableros para scrum o para Kanban.

### Asana
Herramienta de colaboraci�n para la gesti�n general de proyectos.

- OK, porque es capaz de crear paneles personalizables.
- OK, porque es capaz de sincronizarse con otras herramientas.
- OK, porque su interfaz es sencilla.
- OK, porque tiene una versi�n gratis.
- No OK, porque no soporta diferentes metodolog�as agiles.
- No OK, porque no puede llevar una gesti�n de errores eficiente

### BitBucket
Herramienta para el control de versiones y cambios.

- OK, porque adem�s de Git puede utilizar otros repositorios.
- OK, porque tiene versi�n OpenSource.
- OK, porque tiene permisos para las ramas.
- No Ok, porque la versi�n gratis, el m�ximo de almacenamiento es 1Gb

### GitHub
Herramienta para el control de versiones y cambios.

- OK, porque tiene versi�n OpenSource
- OK, porque tiene repositorios privados ilimitados (con menos de 3 contribuidores)
- OK, porque el almacenamiento del repositorio es grande (100 Gb)
- No OK, porque solo permite Git.

## Links
----------------------
- [Jira](https://www.atlassian.com/es/software/jira)
- [Bitbucket](https://bitbucket.org/product/)
- [Azure DevOps](https://azure.microsoft.com/es-es/services/devops/)
- [Trello](https://trello.com/es)
- [Bugzilla](https://www.bugzilla.org/)
- [Asana](https://asana.com/es)
- [GitHub](https://github.com/)
