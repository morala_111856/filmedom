# Integraci�n Continua
- Estado: Aceptado
- Participantes de la decisi�n: Javier Diez Javier Morala
- Fecha: 2020-05-09

## Contexto y Problema
----------------------
Hemos decidido emplear la integraci�n continua para el desarrollo de nuestra aplicaci�n. Esto
quiere decir que necesitaremos automatizar el proceso de a�adir una nueva funcionalidad.

Los pasos de los que consta este proceso son los siguientes:

- Hacer el commit con el nuevo c�digo.
- Crear la imagen Docker.
- Hacer el �run� de la imagen.
- Test de correcto funcionamiento.

Para ello debemos decidir qu� herramienta de gesti�n de la integraci�n continua vamos a usar.

## Controladores de la Decisi�n
----------------------
Somos un equipo de tres personas, por lo que no necesitamos que la herramienta soporte a
muchos usuarios.

Vamos a dar prioridad al precio, ya que contamos con poco presupuesto

## Opciones Consideradas
----------------------
- Jenkins
- Travis CI
- Bamboo
- Circle CI
- CruiseControl

## Decisi�n
----------------------
Opci�n elegida: Jenkins, porque es una herramienta completamente gratuita que soporta
despliegue continuo y es compatible con BitBucket. Adem�s, existen contenedores de Docker con
servidores de jenkins completamente funcionales.

### Consecuencias Positivas
- Es gratuita, por lo que no supondr� ning�n gasto.
- Soporta despliegue continuo, con lo que agilizaremos el desarrollo.

### Consecuencias Negativas
- Ninguno de integrantes del equipo tiene experiencia con esta herramienta y algunos
usuarios afirman que su interfaz es poco intuitiva, por lo que tendremos que dedicar un
tiempo a aprender su funcionamiento.

## Pros y Contras de las Opciones
----------------------
### Jenkins
Una de las herramientas de integraci�n continua m�s conocidas. Est� escrito en Java y se ha
continuado desarrollando desde el a�o 2005. Cuenta con numerosas funciones que asisten no solo
en la integraci�n continua, sino tambi�n en el despliegue y la entrega continua.

- OK: Despliegue y entrega continua.
- OK: M�s de 1000 plugins.
- OK: Gratis.
- OK: Compatible con BitBucket.
- No OK: Interfaz poco intuitiva

### Travis CI
Es una herramienta de integraci�n continua trabaja en estrecha relaci�n con GitHub. Se puede
configurar con un sencillo archivo YAML que se guarda en el directorio ra�z del proyecto. GitHub
informa a Travis CI de todos los cambios efectuados en el repositorio y mantiene el proyecto
actualizado.

- OK: Configuraci�n sencilla.
- OK: Gratis para proyectos de c�digo abierto
- No OK: Precio elevado para el alcance del proyecto (69 - 489 $/mes)
- No OK: No soporta entrega continua

### Bamboo
Herramienta ofrecida desde 2007 por la compa��a Atlassian. No solo sirve de ayuda en la
integraci�n continua, sino tambi�n para funciones de despliegue y gesti�n de lanzamientos.
Funciona a trav�s de una interfaz web.

- OK: Gran cantidad de add-ons.
- OK: Pago �nico.
- No OK: Precio elevado para el alcance del proyecto (10 - 126.500 $)

### Circle CI
CircleCI funciona tanto con GitHub como con Bitbucket. En las fases de prueba, pueden
emplearse tanto contenedores como m�quinas virtuales. CircleCI confiere mucha importancia
a la ejecuci�n de procesos de desarrollo sin interferencias, por lo que arroja de forma
autom�tica builds compatibles con otros entornos.

- OK: Compatible con GitHub y Bitbucket.
- OK: Soporta entrega continua.
- No OK: Precio elevado para el alcance del proyecto (50 - 3.150 $)

### CruiseControl
Una de las aplicaciones m�s antiguas de integraci�n continua. Sali� al mercado en 2001 y ha
continuado desarroll�ndose desde entonces. Los desarrolladores tienen a su disposici�n
numerosos plugins que les facilitar�n el trabajo.

- OK: Gratis.
- OK: De c�digo abierto.
- No OK: No soporta entrega continua.

## Links
----------------------
- [�Cu�les son las mejores herramientas de integraci�n continua?](https://www.ionos.es/digitalguide/paginas-web/desarrollo-web/herramientas-de-integracion-continua/)
