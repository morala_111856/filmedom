# Plan De Sistemas
- Estado: Aceptado
- Participantes de la decisión: Javier Diez, Javier Morala
- Fecha: 2020-06-01

## Contexto y Problema
----------------------
Se tiene la necesidad de construir un plan que defina como debe ser desplegada la aplicación de forma que se cumplan los requisitos definidos en el documento
de despliegue. Estos requisitos y alguno adicional se definen en el siguiente apartado

## Controladores de la Decisión
----------------------
- La aplicación debe de ser escalable
- La aplicación debe de estar en un entorno de alta disponibilidad
- El precio afectará a la decisión

## Opciones Consideradas
----------------------
- AWS
- Data Center privado

## Decisión
----------------------
Opción elegida: **AWS**, porque nos asegura los requerimientos establecidos para nuestra apliación junto con un precio relativamente asequible desde nuestro primer
despliegue. Además nos ofrece las ventajas de trabajar en la nube y nos permite centrarnos en el desarrollo de la aplicación principalmente.

### Consecuencias Positivas
- Facilidad en el despliegue.
- Nos despreocupamos de los costes indirectos (Electricidad, remplazo de hardware, etc.).
- El escalado es automático
- El SLA de AWS asegura un 99.99% del tiempo de disponibilidad.
- El precio inicial para su despliegue es mucho menor al de montar un Data Center propio

### Consecuencias Negativas
- Se pierde el control total sobre nuestros datos
- Sujetos a la disponibilidad de Amazon

## Pros y Contras de las Opciones
----------------------
### AWS
Para poder analizar correctamente las ventajas e inconvenientes que presenta AWS para nuestra aplicación, se va a tener en cuenta la arquitectura
que se pretende usar si, finalmente, se decide usar esta opción.

La arquitecura se resume en el siguiente diagrama

![Arquitecutra AWS](Assets/AWS_Arquitectura.png)

En este diagrama se puede observar que se han dividido la aplicacion en tres secciones, una por servicio y cada uno de estos se encuentra en una
VPC diferente.

La VPC de Aplicación es la que se encuentra expuesta a la red y es la responsable de responder a todos los clientes que se conecten, por tanto,
se ha elegido maquinas **C5n** en este caso, porque estas son idóneas para aplicación que requieran capacidades informáticas elevadas (Responder a la mayor capacidad
de peticiones posible) y ademas ofrecen un ancho de banda elevado (25Gbps). Además, se encuentran en dentro de un gurpo de autoescalado dividido dentro de las
diferentes zonas de disponibilidad de eu-west-1.

En el caso de los otros dos servicios, la forma de realizar el auto escalado y las zonas de disponibilidad son la misma, la diferencia con la anterior VPC es que
estas son privadas, solo accesibles desde las máquinas de las aplicaciones. También, la máquina elegida para utilizar en estos casos son las **T3**, ya que estos
servicios requieren menor capacidad de cómputo al ser las APIs a las que se conecta la aplicación. En este caso, aunque el grupo de auto escalado está pensado para dos
máquinas, estas pordrían aumentar más, debido a su bajo coste mensual.

Con la configuración actual, el coste estimado es de 284.12 mensuales

#### Pros y contras

- OK, porque la alta disponibilidad esta asegurada al utilizar AWS
- OK, porque al ser un servicio en la nube, nuestra accesibilidad a el es muy grande, en lugar de tener la necesidad de comprar hardware para dar el servicio
- OK, porque el escalado de la aplicación se realiza automáticamente dadas unas reglas
- OK, porque AWS nos permite escalar nuestra aplicacion aumentando nuestra disponibilidad, al distribuir las máquinas en distintas zonas de disponibilidad.
- No OK, el precio, aunque no es muy elevado, al ser una empresa pequeńa (2 personas) y ser nuestro único servicio actualmente, es necesaria una inversión
a largo plazo para su despliegue.
- OK, la inversión necesaria para su mantenimiento, se extiende a lo largo del tiempo.
- No OK, porque es menos seguro tener las BBDDs en la nube por temas de seguridad.

### Data Center

Si montaramos el Data Center siguiendo la estructura que hemos diseñado para AWS será necesario comprar dos servidores en los que ejecutaríamos la app y cuatro, 
menos potentes en los que alojaríamos la APIs. Para los servidores, se ha decidido comprar un armario rack con capacidad adicional a los servidores necesitados,
de forma que se puedan extender en un futuro. Es un rack de 12 unidades para servidores de 19", de las que necesitamos 6 para los servidores y 2 para los routers
comprados.
 
Para controlar el tráfico interno hay que comprar, al menos, router y cableado para conectar las diferantes máquinas.

Como gastos mensuales hay que tener en cuenta que hay que adquirir antivirus para cada uno de los equipos y que su licencia caduca. El gasto más importante será 
el de la factura eléctrica, ya que será necesario contratar a dos compañías diferentes para asegurar una alta disponibilidad. También debemos contratar el acceso 
a internet y tener en cuenta que será necesario refrigerar los equipos.

Con el fin de conseguir los requerimientos propuestos en el documento de despliegue, ademas de seguir la arquitectura establecida en AWS y traducirla a un 
Data Center en local, es necesario tener en cuenta aspectos pasivos con el fin de aumentar la disponibilidad. Es decir, para evitar posibles cortes de luz
que interrumpan nuestro servicio, se debe de tener contradada dos tomas de corriente diferentes y los servidores irán conectados la mitad a cada una de las
tomas. Con respecto a la conexion a la red, debe ser algo parecido, diferentes entradas de fibra para evitar interrupciones totales. Por este motivo, también
se ha decidido el comprar dos routers diferentes.

Para la escalabilidad, si no es suficiente con los 6 servidores comprados, se pueden crear VLANs y maquinas virtuales para escalar nuestro servicio aún más.

#### Gasto inicial

- Armario rack: [179€](https://www.pccomponentes.com/digitus-mural-12u-643-x-600-x-600-mm-negro)
- Servidores para las API: [4x678.99€](https://www.pccomponentes.com/hpe-proliant-dl20-gen10-intel-xeon-e-2124-8gb)
- Servidores para la aplicación: [2x1051.01€](https://www.pccomponentes.com/dell-poweredge-r240-intel-xeon-e-2134-16gb-1tb)
- Routers: [2x317.44€](https://www.pccomponentes.com/ubiquiti-unifi-security-gateway-pro-5-puertos-gigabit-2-sfp)
- Cableado: ~50€

Además de este coste inicial de la compra y montaje del Hardware, el montar un Data Center propio implica unos gastos indirectos adicionales y mensuales,
estos son la electricidad, internet, refrigeración, mantenimiento y limpieza de la sala, que puede llegar a suponer unos gastos muy altos.

#### Gastos mensuales

- Antivirus: Kaspersky Total Security 2020, 2,5€ / mes por unidad.
- Factura eléctrica: ~150€
- Factura internet: ~50€
- Refrigeración: ~10€

El gasto inicial asciende a alrededor de 5600€ y el gasto mensual a 200€. Teniendo en cuenta que el gasto mensual en AWS sería de 284.12€ mensuales, la opción del 
data center tardaría unos 6-7 años en ser mas rentable que AWS.

#### Pros y contras

- No OK, porque requiere una inversión muy alta en el primer despliegue.
- OK, porque una vez hecha la inversión incial, los gastos mensuales son menores y puede llegar a amortizarse.
- Ok, porque es mas seguro tener la información en local que en la nube.
- No OK, porque el mantenimiento de los equipos depende de nosotros, y puede suponer un gasto de esfuerzo y dinero importante
- No OK, porque con el fin de conseguir la disponibilidad que ofrece AWS seria necesaria una inversión aun mayor

## Links
----------------------
- [AWS SLA](https://aws.amazon.com/es/compute/sla/)
- [AWS Instances](https://aws.amazon.com/es/blogs/aws/choosing-the-right-ec2-instance-type-for-your-application/)
- [Types of AWS instances](https://aws.amazon.com/es/ec2/instance-types/#instance-details)
