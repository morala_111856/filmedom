﻿# FILMEDOM
Se pretende crear una aplicación web compuesta por microservicios en donde los usuarios de esta
puedan llevar un registro de las series y películas que han visto, así como de las que tienen pendientes
de ver.

De esta forma, las series deberán poder dividirse por temporadas y capítulos, para que los usuarios
tengan una mayor gestión sobre la aplicación.

## Índice
---------------------
[TOC]

## Alcance 
---------------------
Para generar una primera versión funcional de la aplicación, se pretende llegar hasta que a partir de
una interfaz web los usuarios sean capaces de llevar una gestión básica de diferentes series y
películas. Por gestión básica se entiende poder registrar un título como visto o como pendiente de
ver.

En una segunda versión, se quiere ampliar el microservicio de las series para que contemple los
diferentes capítulos y temporadas de estas.

Por último, se quiere generar una aplicación móvil con la misma funcionalidad que la web. Este paso
solo se realizará en un futuro si es posible.

De esta forma, el esquema de la aplicación será el siguiente:

![Esquema proyecto](Assets/EsquemaProyecto.png)

## Procesos de la aplicación
---------------------
Para el desarrollo e integración de la aplicación hace falta definir una serie de procesos. El primer
paso es definir los procesos relacionados con el equipo del proyecto, estos son:

### Proceso de comunicación
Uno de los aspectos más importantes dentro de lo que es el desarrollo del proyecto en todas sus
fases es la comunicación entre los diferentes roles del equipo de trabajo.

Para una correcta implementación de todos los demás procesos es necesario disponer de las
herramientas necesarias su realización, y aún más en este momento, con el actual estado de alarma
que impide a nuestro equipo salir de casa y, por tanto, el teletrabajo en el proyecto.

Por tanto, se propone realizar un mínimo de una reunión semanal en la que esté presente el equipo
completo. En esta reunión se deberá poner al corriente al resto del equipo sobre que ha estado
realizando esa semana y cuáles son sus planes para la siguiente semana.

### Proceso de documentación
Para cualquier decisión del proyecto y desarrollo de este, es necesaria una documentación que
permita en un futuro entender el porqué de las decisiones y cómo se han realizado todos los
proyectos.

En este caso, toda documentación debe llevar el siguiente proceso:

1. Registrar e informar al resto del equipo de que se está trabajando en ese documento.
2. Realizar un borrador del documento.
3. Pasar a la corrección del documento.
4. Revisión del documento por parte del resto del equipo (Si no es correcto, volver al paso ii y
realizar un borrador con los cambios).
5. Publicación o almacenamiento del documento.

### Proceso de reparto de tareas
Antes de ponerse a desarrollar la aplicación es necesario que el equipo entero sepa que tienen que
hacer y que están haciendo el resto del equipo. Se necesita un proceso que se encargue de este
problema, el cual se puede dividir en dos fases:

**Proponer las tareas**, preferiblemente que duren una o dos semanas

1. En una reunión en la que esté el equipo completo, recapitular lo que está hecho en el
proyecto y las tareas anteriores que no se han podido terminar.
2. Obtener nuevas tareas a realizar.
3. Estimar la duración de todas las nuevas tareas (y reestimar la duración de las antiguas)
4. Apuntar en un lugar al que todo el equipo tenga acceso todas las tareas obtenidas en la
reunión. La duración de estas tareas debe de ser corta.

**Elegir las tareas a realizar** por cada miembro del equipo:

1. En la reunión, definir cuál va a ser la primera tarea en la que trabajar.
2. Apuntar la elección en un lugar que todo el equipo tenga acceso.
3. Si se termina la tarea, escoger una nueva de la pila de tareas a realizar que no las ha elegido
nadie.

Por otra parte, están los procesos relacionados con la integración y desarrollo del proyecto.

### Proceso de desarrollo
Como cualquier otro proyecto software, es necesario programar una serie de servicios que
contengan la funcionalidad del proyecto deseado. En este proceso cada desarrollador trabaja con su
parte por separado, y después es necesario poner todo en común para que la aplicación funcione.

En este sentido, el desarrollo del proyecto debe de llevar el siguiente proceso:

1. Si no se ha trabajado en el proyecto, clonar este de la nube.
2. Al implementar una funcionalidad, obtener todos los cambios de los compañeros.
3. Resolver los conflictos de código si los hubiera.
4. Asegurarse de que el proyecto se ejecuta correctamente después de integrar los cambios.
5. Subir los cambios a la nube.

### Proceso de pruebas
Para asegurar la calidad del código generado y el proyecto desarrollado, es necesario realizar una
serie de pruebas para asegurar un correcto funcionamiento.

1. Comprobar que el código se compila sin problemas.
2. Desplegar la aplicación (puede hacerse en local).
3. Comprobar, para cada una de las funcionalidades implementadas, que dan la respuesta
esperada.

### Proceso de despliegue
Una vez completado el servicio que se quiere ofrecer, es necesario realizar un despliegue de este, es
decir, realizar el proceso para que los usuarios sean capaces de acceder a nuestro servicio estén
donde estén (al servirse por internet).

Por esto, es necesario realizar un proceso que defina los pasos a realizar el despliegue de la
aplicación:

1. Descargar el código en la máquina que hará de servidor.
2. Configurar una dirección IP pública para nuestra aplicación.
3. Crear la imagen Docker.
4. Inicializar la base de datos en caso de que sea necesario.
5. Hacer el “run” de la imagen del contenedor Docker.

### Proceso de integración continua
En el proyecto, se requiere de trabajar con un proceso de integración continua. Este proceso debe
de tener en cuenta como se quiere realizar el despliegue y las pruebas del proyecto:

1. Descargar el código en la máquina que hará de servidor.
2. Hacer el commit con el nuevo código.
3. Crear la imagen Docker.
4. Hacer el “run” de la imagen.
6. Test de correcto funcionamiento.

## Recursos
---------------------
En cuanto a los recursos humanos, contamos con un equipo de tres personas: Javier Morala, Verónica
Santos y Javier Diez.

En cuanto a recursos hardware contamos con tres máquinas virtuales alojadas en el servidor de la
universidad. Conforme avance el proyecto se estudiará la opción de añadir más recursos.

## Metodología
---------------------
En lo que se refiere al planteamiento de la aplicación como una serie de microservicios y según se
han identificado los diferentes procesos a seguir en el desarrollo del proyecto y relacionados con el
equipo de este, se cree que la mejor forma de realizar el desarrollo es utilizando una metodología
ágil.

Dados los procesos de comunicación y reparto de tareas, las metodologías a utilizar que más encajan
en este sentido son Scrum y Kanban:

- Se va a seguir el marco de referencia de **Scrum**.
- Para la gestión de tareas de forma visual se va a utilizar **Kanban**.

Actualmente, al no poder realizar reuniones presenciales, para la realización de las reuniones y para
la comunicación del equipo, se ha decidido utilizar la plataforma [Discord](https://discordapp.com/).

En esta plataforma, podremos comunicarnos a través del canal de voz o mediante texto en caso de
haya problemas de conexión (no es posible realizar reuniones físicas debido a la situación actual).
Además, Discord posibilita el compartir la pantalla del ordenador de un usuario con los demás que
estén en la llamada.

Tal y como dice el proceso de comunicación se realizará como mínimo una reunión semanal por
Discord. Esta será los miércoles a las 15:00 (O después de clase). Se ha elegido este día porque es a
mitad de semana y por lo tanto los miembros del equipo tienen las tareas frescas (no con un fin de
semana de por medio) y pueden elegir trabajo a realizar para lo que queda de semana.

Como se ha decidido utilizar **scrum** como marco de referencia, se debe definir la duración de los
sprints, la cual será de **dos semanas**, en donde la reunión de sprint sustituirá la semanal. Una vez
terminadas estas reuniones se actualizará también el **Kanban**.

## Gestión de la configuración
---------------------
Para llevar una gestión de la configuración eficaz en nuestro proyecto en conforme con los
procesos definidos, es necesario un software accesible por todo el equipo donde este sea capaz de
ver todas las tareas que hay que realizar y actualizarlas para asignárselas a un trabajador.

También es necesario para el desarrollo una forma de gestionar las versiones y que todo el equipo
pueda tener acceso para descargar y subir los cambios de los servicios desarrollados.

Para la realización de estos procesos se ha decidido utilizar el software **Jira** junto con **Bitbucket**. La
decisión de porque utilizar estos softwares frente a otros, queda recogido en el documento ADR
[Gestión de la Configuración](ADR/GestionDeLaConfiguracion.md).

## Integración continua
---------------------
Vamos a seguir el modelo de integración continua para el desarrollo de nuestra aplicación. Esto
quiere decir que cada funcionalidad desarrollada deberá ser integrada inmediatamente al proyecto.
Para que este modelo sea eficiente el proceso de integración tendrá que estar automatizado.

Vamos a utilizar la herramienta [Jenkins](https://jenkins.io/) para esto ya que es totalmente gratuita y nos brinda todas
las herramientas que necesitamos. Además, es compatible con BitBucket. La justificación de esta
decisión se encuentra en el ADR de [Integracion Continua](ADR/IntegracionContinua.md).

Aún no se ha definido en qué consistirán las pruebas de correcto funcionamiento.

### Versión 0
Como forma de comprender el funcionamiento de Jenkins, así como aprender a utilizarlo, se ha creado una pipeline
en jenkins. Esta pipeline es muy básica, su objetivo es realizar el build de una aplicacion en nodejs.

Para la creación de la pipeline se ha conectado jenkins con el repositorio de Bitbucket y creado un archivo
**Jenkinsfile**, el cual difine la pipeline. Cada vez que se ejecuta manualmente, descarga la última versión de Bitbucket
con el Jenkinsfile correspondiente y realiza los pasos definidos en este:

- Moverse a la carpeta de la aplicación.
- Instalar los paquetes necesarios.
- Realizar el build de la aplicación.

### Versión 1
Con la versión anterior se ha aprendido el funcionamiento de Jenkins, ahora se pretende realizar una pipeline que compile
y ejecute el proyecto desarrollado. Además, debe lanzarse automáticamente siempre que se realice un commit en Bitbucket. Esto
último no es posible, por lo que cada cierto tiempo (1 hora) jenkins comprobará el repositorio por si ha habido nuevos commits, y,
en caso afirmativo, ejecutara la pipeline.

El esquema pensado para la pipeline en esta versión es el siguiente:

![Pipeline versión 1](Assets/Fase1.png)

En esta versión y como se observa en la imagen de la estructura de la pipeline, la app está dividida en dos partes diferenciadas:

- **El servicio del frontend**, es la parte que verá el cliente y actualmente se ha añadido el catálogo de películas. Utiliza Angular 9.
- **El servicio de películas**, es un servidor node que escucha una serie de rutas y devuelve, actualmente, un listado de películas.

En la rama de **Angular**, el primer paso es instalar los paquetes necesarios, realizar el build de la aplicación con la etiqueta `--prod`
y crear y ejecutar el docker de la aplicación. En el caso del servicio de **Películas**, se debe instalar las librerías necesarias,
compilar el código typescript en código javascript y crear y ejecutar el archivo docker.

### Versión 2

En esta segunda y definitiva versión, se ha modificado el proceso de integración para poder realizar tests unitarios de nuestra aplicación.

Se ha decidido testear unicamente el funcionamiento correcto del servicio de películas, el cual sería la API y el servicio mas importante entre
los dos implementados. Se realizan dos pruebas:

- Primero se lanza una petición POST que comprueba que se pueden añadir nuevas películas.
- Se realiza una petición GET al objeto que se acaba de añadir en la petición anterior.
- Si todo ha funcionado correctamente se devuelve un OK y se continua con el proceso, en caso contrario
saltará una excepción que hara que la pipeline falle.

## Herramientas que utilizar
---------------------
Para crear esta aplicación vamos a necesitar una base de datos, donde se guardará toda la
información de series y películas para luego ser utilizadas.

Para desarrollar esta base de datos utilizaremos:

- mongoDB
*Mlab
*nuestros servidores(upna)

También desarrollaremos 3 APIS para nuestra aplicación tanto para usuarios, películas y series, cada
una de ellas servirán para la comunicación con las bases de datos y así entregar un resultado a través
de una página web, además se puede extender para aplicaciones móviles.

Para el desarrollo de las API y la WEB utilizaremos lenguajes e IDEs como:

- Angular
* TypeScript
* AngularCLI
* Angular Material
* Visual Studio Community
- APIs y Backend
* C# MVC
* Visual Studio Community

## Estimación
---------------------
El proyecto se puede dividir en tres fases, una primera en la que se definan todos los procesos, una
segunda donde se prepare toda la estructura de la integración continua y del proyecto. Por último,
desarrollar los diferentes servicios que se ofrecen.

Según esta división del proyecto se estima:

- Definición del proyecto: 2 semanas
- Preparación de la estructura: 2 semanas
- Desarrollo del proyecto: 4 ~ 6 semanas
* Desarrollo de los microservicios: 2 semanas
* Desarrollo de los frontales: 2 semanas
* Comunicación frontal y microservicios: 1 semana

Esto hace un total de **8 ~ 10 semanas** para completar el proyecto. Estos tiempos son solo una
estimación y pueden extenderse durante los diferentes sprints.

## Despliegue y Plan de Sistemas
---------------------
>El despliegue de la aplicación se realizará siguiendo los pasos definidos en el proceso de despliegue.
Para poder realizar este despliegue necesitaremos unas máquinas que podamos utilizar como
servidor. Para ello utilizaremos las máquinas virtuales proporcionadas por la universidad. A lo largo
del desarrollo del proyecto se estudiará la posibilidad de desplegar el proyecto en AWS.

Se han establecido una serie de requisitos de despliegue en el documento [Despliegue](Despliegue.md). En 
estos se detalla que las decisiones que se tomen en el proyecto deben conseguir que este pueda ser escalable
y ademas, que se encuentre deplegado con una alta disponibilidad.

De esta forma, se ha desarrollado un ADR con el [Plan de Sistemas](ADR/PlanDeSistemas.md). En este se detalla
el porqué de elegir realizar el despliegue sobre AWS en lugar de montar un Data Center propio.

## Problemas en el desarrollo y solución
--------------------- 
- Utilizar Jenkins con node: Instalar un plugin de jenkins para manejar las herramientas que utiliza en la
ejecución de la pipeline. Otra solución podría haber sido realizar un agente de jenkins en un docker con
node instalado.
* Estado: Solucionado (5h).
- Creacion de una imagen docker dentro de una pipeline. La imagen debe generarse en el host. Por ahora se ha
probado a utilizar el plugin de Docker en jenkins para comunicarse con el Docker Daemon.
* Estado: Solucionado. (7.5h).
* Solución: Despues de investigar y probar diferentes configuraciones, se ha llegado a la solución. Esta ha sido
utilizar el plugin de jenkins para Docker (El cual se instala automaticamente al instalar BlueOcean). Además ha
sido necesario exponer el Daemon de Docker para que Jenkins, el cual tiene su propio Docker, pueda conectarse al
Daemon por una conexión TCP. Además hay que configurar el plugin de Jenkins correctamente y utilizar una imagen
que permita conectarse a Docker por SSH. Una vez hecho esto, se define en la pipeline el agente que nos permite
realizar esta conexión SSH
- Implementacion de los Tests. Nuestro problema ha sido la implementación de los test como parte del proceso de
la pipeline
* Estado: Solucionado (4h).
* Solución: Al final, se ha utilizado un script test en node.js el cual para hacer las peticiones se conecta
a la maquina host a través de su IP privada en la interfaz eth0.

## Manual de usuario de la aplicación
Para este manual de usuario de la aplicacion se va a tener en cuenta que la aplicación está desplegada y accesible
para todos los usuarios.

Una vez se accede a la URL de la aplicación, la primera pantalla que aparece es la siguiente

![Manual de usuario home](Assets/Manual_de_usuario_I.png)

En esta pantalla, el usuario debe de elegir si quiere acceder al catálogo de películas o al catálogo de series. Para
poder acceder basta con hacer click en la palabra de la categoría deseada. Además con el fin de ser mas intuitivo para
el usuario final, tiene una animación de selección.

Una vez se accede a una de las dos categorías, se muestra un catálogo de diferentes títulos, los cual se puede recorrer
y ver su información. Esta pantalla corresponde a la siguiente imagen.

![Manual de usuario catálogo](Assets/Manual_de_usuario_II.png)

Además de poder ver el catálogo se ha implementado la opción de añadir nuevas peliculas a este, para ello, el usuario deberá
de hacer click en el botón morado en la parte de arriba a la derecha. Este botón abrirá un diálogo con el siguiente formulario

![Manual de usuario añadir](Assets/Manual_de_usuario_III.png)

Una vez rellenado el formulario, en el cual es necesario completar los tres campos para poder añadir un nuevo título, este
se cerrara y se añadirá al catálogo actual, de forma que si, volvemos a la página principal y se vuelve a acceder al catálogo en
que se han añadido el título, este permanecera en el catálogo.

## Demo de la aplicación
Se ha subido una demo del funcionamiento de la aplicación a YouTube en el siguiente link https://youtu.be/1NpCVEoMdos.

## ADRs
---------------------
- [Gestión de la Configuración](ADR/GestionDeLaConfiguracion.md)
- [Integración Continua](ADR/IntegracionContinua.md)
- [Plan de Sistemas](ADR/PlanDeSistemas.md)