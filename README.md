# Proyecto Filmedom #
---------------------
Este es el repositorio del proyecto Filmedom, creado y mantenido actualmente por **Javier Diez**, **Ver�nica Santos** y **Javier Morala**.

Se pretende desarrollar una aplicaci�n web basada en una arquitectura de microservicios, esta consiste un catalogo de series y pel�culas. El desarrollo de la aplicacion se lleva mediante un proceso de integraci�n continua.

### �Para qu� esta pensado este repositorio? ###

* Llevar un control de versiones de la aplicacion que se pretende desarrollar.
* Llevar un control de versiones de la documentaci�n de la aplicacion desarrollada
* Utilizar el repositorio como punto de entrada para poder realizar una integraci�n continua.

### �Cu�l es la estructura del repositorio? ###

* General
	- FilmedomMicroServices 
	> Codigo de la aplicaci�n
		* Presentacion
		> Servicio para la UI de la aplicacion. Utiliza angular.
		* CatalogoSeries
		> Servicio para el cat�logo de las series
		* CatalogoPeliculas
		> Servicio para el cat�logo de las pel�culas
	- Documenaci�n
	> Carpeta donde se almacena la documentaci�n
		* Filmedom.md
		> Documento principal
		* ADR
		> Los diferentes ADR creados para la aplicaci�n
		* Assets
		> Galeria de imagenes utilizada en la documentaci�n
	- Jenkinsfile 
	> Pipeline de Jenkins para la integraci�n continua

### Tareas por hacer ###

- [x] Creaci�n de la estructura del proyecto
- [x] Cambiar la documentaci�n actual a formato markdown
- [ ] ADR de documentaci�n
- [ ] ADR de lenguajes
- [ ] ADR de estructura del proyecto
- [x] Frontend sencillo de la aplicacion con angular
- [ ] ADR de base de datos utilizada
- [ ] Crear API catalogo series
- [x] Crear API catalogo pel�culas
- [x] Crear pipeline de esta versi�n
- [ ] V2
	* [ ] Conectar API pel�culas con BD
	* [ ] Conectar API series con BD
	* [ ] Crear las pruebas
	* [ ] Modificar pipeline para a�adir las pruebas

### Links y otra documentaci�n �til ###

* [Pipeline en Jenkins](https://www.jenkins.io/doc/tutorials/create-a-pipeline-in-blue-ocean/)
* [Docker y pipelines con Jenkins](https://www.jenkins.io/doc/book/pipeline/docker/)
* [Aplicaci�n NodeJS con Jenkins](https://www.jenkins.io/doc/tutorials/build-a-node-js-and-react-app-with-npm/)
* [Angular testing](https://angular.io/guide/testing)
* [Ejemplo microservicios con node](https://medium.com/@cramirez92/build-a-nodejs-cinema-microservice-and-deploying-it-with-docker-part-1-7e28e25bfa8b)
* [Angular 9 y NodeJS](https://www.techiediaries.com/universal-angular-express/)
* [Ejemplo microservicios 2](https://www.cuelogic.com/blog/microservices-with-node-js)
* [Ejemplo microservicios 3](https://nodesource.com/blog/microservices-in-nodejs)
* [Angular y docker](https://medium.com/@wkrzywiec/build-and-run-angular-application-in-a-docker-container-b65dbbc50be8)
