import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CatalogoPeliculasComponent } from './Peliculas/catalogo-peliculas/catalogo-peliculas.component';
import { CatalogoSeriesComponent } from './Series/catalogo-series/catalogo-series.component';


const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'catalogo-peliculas', component: CatalogoPeliculasComponent },
    { path: 'catalogo-series', component: CatalogoSeriesComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
