export class PeliculaResumen {
    title: string;
    director: string;
    year: string;
    id: string;
}