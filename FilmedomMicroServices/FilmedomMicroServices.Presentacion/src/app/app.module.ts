import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CatalogoPeliculasComponent, DialogFormularioNueva } from './Peliculas/catalogo-peliculas/catalogo-peliculas.component';
import { CartaPeliculaComponent } from './Peliculas/Componentes/carta-pelicula/carta-pelicula.component';
import { DatosPeliculaComponent } from './Peliculas/datos-pelicula/datos-pelicula.component';
import { CatalogoSeriesComponent } from './Series/catalogo-series/catalogo-series.component';
import { CartaSerieComponent } from './Series/Componentes/carta-serie/carta-serie.component';
import { DatosSerieComponent } from './Series/datos-serie/datos-serie.component';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        CatalogoPeliculasComponent,
        CartaPeliculaComponent,
        DatosPeliculaComponent,
        CatalogoSeriesComponent,
        CartaSerieComponent,
        DatosSerieComponent,
        DialogFormularioNueva
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
