import { Component, OnInit, Input } from '@angular/core';
import { PeliculaResumen } from '../../../Modelos/PeliculaResumen';

@Component({
    selector: 'Filmedom-carta-pelicula',
    templateUrl: './carta-pelicula.component.html',
    styleUrls: ['./carta-pelicula.component.scss']
})
export class CartaPeliculaComponent implements OnInit {

    @Input() pelicula: PeliculaResumen = new PeliculaResumen();

    constructor() { }

    ngOnInit(): void {
    }

}
