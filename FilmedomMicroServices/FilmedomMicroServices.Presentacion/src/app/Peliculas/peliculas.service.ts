import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PeliculaResumen } from '../Modelos/PeliculaResumen';

@Injectable({
    providedIn: 'root'
})
export class PeliculasService {

    constructor(public http: HttpClient) { }

    getPeliculasListado() {
        return this.http.get<PeliculaResumen[]>("http://localhost:5000/peliculas")
    }

    postPelicula(nueva: PeliculaResumen) {
        return this.http.post<PeliculaResumen>("http://localhost:5000/nueva-pelicula", nueva).subscribe(result => {
            if (result)
                return true;
        })
    }
}
