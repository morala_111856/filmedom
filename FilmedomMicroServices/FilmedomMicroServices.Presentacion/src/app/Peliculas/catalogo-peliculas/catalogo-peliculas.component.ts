import { Component, OnInit, Inject } from '@angular/core';
import { PeliculasService } from '../peliculas.service';
import { PeliculaResumen } from '../../Modelos/PeliculaResumen';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'Filmedom-catalogo-peliculas',
    templateUrl: './catalogo-peliculas.component.html',
    styleUrls: ['./catalogo-peliculas.component.scss']
})
export class CatalogoPeliculasComponent implements OnInit {

    peliculas: PeliculaResumen[];

    constructor(public peliculasService: PeliculasService, public dialog: MatDialog) { }

    ngOnInit(): void {
        this.peliculasService.getPeliculasListado().subscribe(response => {
            if (response) {
                this.peliculas = response;
            }
        })
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(DialogFormularioNueva, {
            width: '500px',
            data: new PeliculaResumen()
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');

            console.log(result);
            if (result) {
                this.peliculas.push(result)
                this.peliculas = [...this.peliculas];
                this.peliculasService.postPelicula(result);
            }
        });
    }

}

@Component({
    selector: 'dialog-formulario-nueva',
    templateUrl: 'DialogFormularioNueva.html',
})
export class DialogFormularioNueva implements OnInit {

    constructor(public dialogRef: MatDialogRef<DialogFormularioNueva>,
        @Inject(MAT_DIALOG_DATA) public data: PeliculaResumen, private formBuilder: FormBuilder) { }

    loginForm: FormGroup;
    isSubmitted: boolean = false;

    get formControls() { return this.loginForm.controls; }

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            title: ['', Validators.required],
            director: ['', Validators.required],
            year: ['', Validators.required]
        });
    }

    nuevo() {
        console.log(this.loginForm.value);

        this.isSubmitted = true;
        if (this.loginForm.invalid) {
            return;
        }

        this.data.title = this.loginForm.get("title").value;
        this.data.director = this.loginForm.get("director").value;
        this.data.year = this.loginForm.get("year").value;

        console.log(this.data);

        this.dialogRef.close(this.data);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
