import express = require('express');
import bodyParser = require('body-parser');
 
var pelis = [
  {
    title: 'Raging bull',
    director: 'Martin Scorsese',
    year: '1980',
    id: '1'
  },
  {
    title: 'Once upon a time in america',
    director: 'Sergio Leone',
    year: '1984',
    id: '2'
  },
  {
    title: 'Spirited Away',
    director: 'Hayao Miyazaki',
    year: '2001',
    id: '3'
  },
  {
    title: 'Akira',
    director: 'Katsuhiro Otomo',
    year: '1988',
    id: '4'
  },
  {
    title: 'Pulp fiction',
    director: 'Quentin Tarantino',
    year: '1994',
    id: '5'
  },
  {
    title: 'Bruno',
    director: 'Larry Charles',
    year: '2009',
    id: '6'
  },
  {
    title: 'The Big Lebowski',
    director: 'Joel Coen',
    year: '1998',
    id: '7'
  },
  {
    title: 'The godfather',
    director: 'Francis Ford Coppola',
    year: '1972',
    id: '8'
  },
  {
    title: 'The Godfather: Part II',
    director: 'Francis Ford Coppola',
    year: '1974',
    id: '9'
  },
  {
    title: 'Taxi driver',
    director: 'Martin Scorsese',
    year: '1976',
    id: '10'
  },
  {
    title: 'Gran Torino',
    director: 'Clint Eastwood',
    year: '2008',
    id: '11'
  },
  {
    title: 'Kill Bill: Vol. 1',
    director: 'Quentin Tarantino',
    year: '2003',
    id: '12'
  },
  {
    title: 'Schindlerís List',
    director: 'Steven Spielberg',
    year: '1993',
    id: '13'
  },
  {
    title: '12 Angry Men',
    director: 'Sidney Lumet',
    year: '1957',
    id: '14'
  },
  {
    title: 'The Rocky Horror Picture Show',
    director: 'Jim Sharman',
    year: '1975',
    id: '15'
  },
  {
    title: 'The Good, the Bad and the Ugly',
    director: 'Sergio Leone',
    year: '1966',
    id: '16'
  },
  {
    title: 'Billy Elliot',
    director: 'Stephen Daldry',
    year: '2000',
    id: '17'
  },
  {
    title: 'GoodFellas',
    director: 'Martin Scorsese',
    year: '1990',
    id: '18'
  },
  {
    title: 'Apocalypse Now',
    director: 'Francis Ford Coppola',
    year: '1979',
    id: '19'
  },
  {
    title: 'The Shawshank Redemption',
    director: 'Frank Darabont',
    year: '1994',
    id: '20'
  }
  
]; 
 
const port = 5000;

// Create a new express app instance

const app: express.Application = express();

// Headers
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/peliculas', function (req, res) {

	var pelisSend = pelis;
	
	if(req.query.year){
		pelisSend = pelisSend.filter(peli => peli.year === req.query.year);
	}
	
	if(req.query.director){
		pelisSend = pelisSend.filter(peli => peli.director === req.query.director);
	}
	
	if(req.query.id){
		pelisSend = pelisSend.filter(peli => peli.id === req.query.id);
	}
	
	if(req.query.title){
		pelisSend = pelisSend.filter(peli => peli.title === req.query.title);
	}
	
	if(req.query.order){
		pelisSend.sort((a,b) => (a[req.query.order] > b[req.query.order]) ? 1 : ((b[req.query.order] > a[req.query.order]) ? -1 : 0)); 
	}
	
	res.set('Content-Type', 'application/json')
	res.send(JSON.stringify(pelisSend));
	
});

app.post('/nueva-pelicula', function (req, res){
	console.log(req.body);
	
	var peli = req.body
	peli.id = (parseInt(Math.max.apply(Math, pelis.map(function(o) { return o.id; }))) + 1).toString()
	pelis.push(peli);
	
	res.send('OK');
});

app.listen(port, function () {
console.log('App is listening on port ' + port);
});
